#!/bin/bash
SARGSDEFAULT_='-u'
SARGS_=$SARGSDEFAULT_
if [ $# -gt 0 ]; then
	if [ ! -z "$1" ]; then
		SARGS_=$1
	fi
fi
[ "xx${SARGS_}" == "xx${SARGSDEFAULT_}" ] && printf "%s\n" "$SARGS_"
#/usr/bin/sar -1 ${SARGS_} | /usr/bin/cat - < $(/usr/bin/sar) | python ./check_sactivity.py
RC_=$?
exit $RC_
#/usr/bin/sar -1 -q | /usr/bin/cat - <(/usr/bin/sar -q) | python ./check_sactivity.py
