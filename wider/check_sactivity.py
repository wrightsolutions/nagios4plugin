#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2017, Gary Wright wrightsolutions.co.uk/contact
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the copyright holder nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

""" Nagios plugin for System Activity (sar) output
Two example ways to invoke manually:
sar | python ./check_sactivity.py
...or...
python ./check_sactivity.py -t 30 /tmp/sar.txt
"""

from __future__ import print_function
from datetime import datetime
import fileinput
from os import getenv as osgetenv
#from os import path as ospath
import re
from string import printable
from sys import argv,exit,stdin
import yaml


STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3
STATE_DEPENDENT=4
STATE_UNKNOWN_MESSAGE='STATE UNKNOWN'
TIMEOUT_DEFAULT=24
DESCRIPTION='Check System Activity output'

try:
	from argparse import ArgumentParser, RawTextHelpFormatter
	import subprocess
except ImportError:
	print(STATE_UNKNOWN_MESSAGE)
	exit(STATE_UNKNOWN)

VERSION='0.1'

PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
PYVERBOSITY = 1
if PYVERBOSITY_STRING is None:
	pass
else:
	try:
		PYVERBOSITY = int(PYVERBOSITY_STRING)
	except:
		pass
#print(PYVERBOSITY)

COLON=chr(58)
DOTTY = chr(46)
BRACE_OPEN=chr(123)
BRACE_CLOSE=chr(125)
# Above 12x are curly {}, below 4x are rounded so ()
PARENTH_OPEN=chr(40)
PARENTH_CLOSE=chr(41)
#RE_BRACKETED_TIME_OR_NUM = re.compile(r'\[[\-T0-9.:]*\]')

SET_PRINTABLE=set(printable)
TRANSLATE_DELETE = ''.join(map(chr, range(0,9)))

#RE_PALPHA = re.compile('%[a-z]+',re.IGNORECASE)
RE_PALPHA = re.compile('%[a-zA-Z]+')
RE_INT10 = re.compile('[0-9]{1,10}')
RE_TIME6 = re.compile('[0-9]{2}:[0-9]{2}:[0-9]{2}')
TEXT_FIELD_FALLBACK = 'No detection worked so far so trying str() fallback'
CHECK_CONFIG_FILE = 'sactivity.yaml'
LINES_SAMPLE_SIZE = 5
NAGIOS_STEM_CRITICAL_SA = 'CRITICAL: System Activity'
SA_PASSED_ALL = 'System Activity passed all checks'

class ArgParser(ArgumentParser):
	def error(self, message):
		#self.print_help()
		print(STATE_UNKNOWN_MESSAGE, ': ', message)
		exit(STATE_UNKNOWN)


def parser_initialise():
	try:
		par = ArgParser(description=DESCRIPTION,formatter_class=RawTextHelpFormatter)
	except:
		par = None

	if par is None:
		return par

	par.add_argument('-V', '--version', help='Show plugin version',
		action='store_true')
	par.add_argument('-s', '--sar', help='sar type to query',
		action='store', dest='sartype', default='u', required=False)
	par.add_argument('-l', '--lowestlines', help='Lowest number of field delimited lines to be valid input',
		action='store', dest='lowlines', type=int, default=3)
	par.add_argument('-m', '--maxlines', help='Maximum number of lines to process',
		action='store', dest='maxlines', type=int, default=500)
	par.add_argument('-t', '--timeout', help='(Sub)Process timeout (where input not piped in)',
		action='store', dest='timeout', type=int, default=TIMEOUT_DEFAULT)
	par.add_argument('files', metavar='FILE', nargs='*', help='files of System Activity')

	return par


class SysActivityRow(object):

	""" System Activity Data Row Class
	Subclass this for specialisation such as runq """

	SIGNATURE_U = [6, 3, 2, 2, 2, 2, 2, 2]
	SIGNATURE_U_LABELS = ['time6','cpu','user','nice','system','iowait','steal','idle']
	SIGNATURE_2x3_THEN_6x2 = [3, 3, 2, 2, 2, 2, 2, 2]
	SIGNATURE_Q = [6, 1, 1, 2, 2, 2, 1]
	SIGNATURE_Q_LABELS = ['time6','runq-sz','plist-sz','ldavg-1','ldavg-5','ldavg-15','blocked']
	SIGNATURE_Q_AVERAGE = [3, 1, 1, 2, 2, 2, 1]

	def __init__(self,row,row_signature_previous=None):
		self._row = row
		self._row_list = []
		self._row_signature = []
		self._field_indexes = []
		self._field_indexes_rejected = []
		self._row_list_labels = []
		# Next the lists that are specific to nature of the field
		self._times = []
		self._dec2s = []
		self._ints = []
		self._strings = []
		if row_signature_previous is None:
			self._row_signature_previous = []
		else:
			self._row_signature_previous = row_signature_previous
		self._satype = None
		if row:
			self._satype = self.lists_from_row()
		pass


	""" times property requires no setter as only set directly during row parse (internal). """
	@property
	def times(self):
		return self._times

	""" row property requires no setter as only set directly during row parse (internal). """
	@property
	def row(self):
		return self._row

	""" row_list property requires no setter as only set directly during row parse (internal). """
	@property
	def row_list(self):
		return self._row_list

	""" row_list_labels property requires no setter as only set directly during row parse (internal). """
	@property
	def row_list_labels(self):
		return self._row_list_labels

	""" dec2s property requires no setter as only set directly during row parse (internal). """
	@property
	def dec2s(self):
		return self._dec2s

	""" ints property requires no setter as only set directly during row parse (internal). """
	@property
	def ints(self):
		return self._ints

	""" strings property requires no setter as only set directly during row parse (internal). """
	@property
	def strings(self):
		return self._strings

	""" sar_previous property requires no setter as only set directly during row parse (internal). """
	@property
	def sar_previous(self):
		return self._sar_previous

	""" row_signature property requires no setter as only set directly during row parse (internal). """
	@property
	def row_signature(self):
		return self._row_signature

	""" satype property requires no setter as only set directly during type_from_signature (internal). """
	@property
	def satype(self):
		return self._satype


	def row_signature_append(self,int_given):
		len_before = len(self.row_signature)
		#print(int_given)
		self._row_signature.append(int(int_given))
		return (len(self.row_signature)-len_before)


	def type_from_signature(self,row_signature=None,verbosity=1):
		if row_signature is None:
			row_signature = self.row_signature
		if verbosity > 1:
			print(row_signature)
		if sum(row_signature) < 2:
			return self.satype
		cls = self.__class__
		if cls.SIGNATURE_U == row_signature:
			self._satype = 'u'
		elif cls.SIGNATURE_Q == row_signature:
			self._satype = 'q'
		elif cls.SIGNATURE_2x3_THEN_6x2 == row_signature:
			self._satype = None
		elif cls.SIGNATURE_Q_AVERAGE == row_signature:
			self._satype = None
		else:
			self._satype = None
		return self.satype

	def lists_from_row(self):
		rarray = self.row.split()
		for idx,field in enumerate(rarray):

			try:
				dec2 = round(float(field),2)
				if field == "{:.2f}".format(dec2) or field == str(dec2):
					pass
				else:
					# Not 2 decimal as failed comparison
					dec2 = None
			except:
				dec2 = None

			if dec2 is not None:
				self._field_indexes.append(idx)
				self._dec2s.append(idx)
				self._row_list.append(dec2)
				self.row_signature_append(2)
				continue

			# Try for int() so testing for INTEGER
			try:
				field_int = int(field)
			except:
				field_int = None

			if field_int is not None:
				self._field_indexes.append(idx)
				self._ints.append(idx)
				self._row_list.append(field_int)
				self.row_signature_append(1)
				continue

			try:
				field_t = datetime.strptime(field,'%H:%M:%S')
			except ValueError as e:
				field_t = None

			if field_t is not None:
				self._field_indexes.append(idx)
				self._ints.append(idx)
				self._row_list.append(field_int)
				self.row_signature_append(6)
				continue

			if PYVERBOSITY is None or PYVERBOSITY > 1:
				print(dec2,field,str(dec2))
				print("field {0} - {1}".format(idx,TEXT_FIELD_FALLBACK))


			""" Next is our fallback conversion/test which
			would result in an entry of 3 in row_signature
			if successful
			"""
			try:
				field_str = str(field)
			except:
				field_str = None

			if field_str is not None:
				self._field_indexes.append(idx)
				self._strings.append(idx)
				self._row_list.append(field_str)
				self.row_signature_append(3)
				continue

			self._field_indexes_rejected.append(idx)
			self._row_signature.append(0)


		if PYVERBOSITY is None or PYVERBOSITY > 1:
			print(self.row_list)
			print(self._field_indexes_rejected)

		type_ret = self.type_from_signature(self._row_signature)

		cls = self.__class__
		if 'q' == type_ret:
			# time6,runq-sz,plist-sz,ldavg-1,ldavg-5,ldavg-15,blocked
			self._row_list_labels = cls.SIGNATURE_Q_LABELS
		elif 'u' == type_ret:
			# time6,cpu,user,nice,system,iowait,steal,idle
			self._row_list_labels = cls.SIGNATURE_U_LABELS
		else:
			pass

		return type_ret


class SysActivity(object):

	""" System Activity Data Class
	Argument is an iterable [list/tuple] of lines
	Subclass this for specialisation such as runq """

	TYPES_SELECTED = ['q', 'u']

	def __init__(self,lines):
		self._lines_all = lines
		self._lines = []
		self._line_n_labels = []
		self._sars = []
		self._types_all = []
		self._types = []

	def __enter__(self):
		types_list = []
		line_previous_signature = []
		for line in self.lines_all:
			if set(line).issubset(SET_PRINTABLE):
				sar = SysActivityRow(line,line_previous_signature)
				self.types_all_append(sar.satype)
				if sar.satype in self.__class__.TYPES_SELECTED:
					self.types_append(sar.satype)
					self._lines.append(line)
					self._sars.append(sar)
					self._line_n_labels = sar.row_list_labels
					#print(sar.row_list_labels)
			line_previous_signature = sar.row_signature
		return self


	def __exit__(self, exc_type, exc_val, exc_tb):
		return


	""" lines_all property requires no setter as only set directly during init """
	@property
	def lines_all(self):
		return self._lines_all

	""" lines property requires no setter as only set directly during enter """
	@property
	def lines(self):
		return self._lines

	""" sars property requires no setter as only set directly during enter """
	@property
	def sars(self):
		return self._sars

	""" line_n_labels property requires no setter as only set directly during enter """
	@property
	def line_n_labels(self):
		return self._line_n_labels

	""" types_all property requires no setter as only set directly during
	init and under control of types_all_append() """
	@property
	def types_all(self):
		return self._types_all

	def types_all_append(self,type_given):
		len_before = len(self.types_all)
		self._types_all.append(type_given)
		return (len(self.types_all)-len_before)


	""" types property requires no setter as only set directly during
	init and under control of types_append() """
	@property
	def types(self):
		return self._types

	def types_append(self,type_given):
		""" types here is usually a shorter list than types_all
		Be sure to use the correct append method for your purpose
		"""
		len_before = len(self.types)
		self._types.append(type_given)
		return (len(self.types)-len_before)


	def lines_print(self,strip_both=False):
		for ln in self.lines:
			if strip_both:
				print(ln.strip())
				continue
			print(ln.rstrip())
		return


	def lines_n(self,num):
		if len(self.lines) < num:
			return []
		lines_tail_filtered = []
		num1 = 1+num
		lines_types1 = self.types[:-num1]
		if lines_types1[-1] == lines_types1[-2]:
			# ultimate and penultimate are same type
			lines_tail_filtered = self.lines[-num:]
			#print(num,num,num,lines_tail_filtered)
			
		elif lines_types1[-2] == lines_types1[-3]:
			# Last is probably an average so ignore it
			lines_tail_filtered = self.lines[-num:]
			lines_tail_filtered.pop()
		else:
			pass
		return lines_tail_filtered


	def lines_n_print(self,num=5,strip_both=False):
		lines_n = self.lines_n(num)
		for ln in lines_n:
			if strip_both:
				print(ln.strip())
				continue
			print(ln.rstrip())
		return lines_n


	def field_check(self,label,warn_thresh,crit_thresh):
		return


	def field_checks(self,config_checks_dict):
		""" Returns a four tuple of texts
		warn_text is third field (default is '')
		crit_text is fourth field (default is '')
		Suitable return for a test like sum(fc4[:1]) > 0
		"""
		warn_text = ''
		crit_text = ''
		for label in self.line_n_labels:	
			if label in config_checks_dict:
				idx = self.line_n_labels.index(label)
				#print(idx)
				warn = -1
				crit = -1
				rows = 3
				try:
					check_spec = config_checks_dict[label]
					warn = check_spec['warn']
					crit = check_spec['crit']
					rows = check_spec['rows']
				except:
					pass
				if 1 > (warn+crit):
					""" Could in theory be quitting here for 0.25+0.50 but
					more likely to be quitting correctly """
					continue

				val = 0
				lines_len = self.lines_n(rows)
				if lines_len < 1:
					continue
				elif lines_len < rows:
					# lines but with a pop()
					pass
				else:
					sars_n = self.sars[-rows:]
					for sar in sars_n:
						val = sar.row_list[idx]
				if crit > 0:
					try:
						cr = float(crit)
					except ValueError:
						cr = None
					if cr is not None and val > cr:
						#print(cr,val)
						crit_text += "{0} exceeds Crit for {1}. ".format(val,label)

				if len(crit_text) > 1:
					break

				if warn > 0:
					try:
						wa = float(warn)
					except ValueError:
						wa = None
					if wa is not None and val > wa:
						# Overwriting by using = rather than += here
						warn_text = "{0} exceeds warn for {1}. ".format(val,label)
						
		"""
				#[ print(sar.row_list) for sar in sars_n ]
				#print(rows,sars_n)
				print(warn)
				print(config_checks_dict)
				field_check(sa.line_n_labels.index(label),
					cconfig_check[label])
		"""
		return (len(warn_text),len(crit_text),warn_text,crit_text)


def preprocess_lines(lines,linelimit=0,linecount_sample=20,verbosity=0):

	prep_return = 0

	lines_passing = []
	
	for idx,line in enumerate(lines):

		#print("processing line %d" % idx)
		line_rc = 0

		if set(line).issubset(SET_PRINTABLE):
			lines_passing.append(line)
		else:
			#line_rc = 150
			prep_return = 200
			break

		if linelimit < 1:
			pass
		else:
			if linelimit < (1+idx):
				break

	return lines_passing


def fileinput_lines(input_given,linelimit=0,linecount_sample=20,verbosity=0):
	lines_to_prep = input_given
	lines = preprocess_lines(lines_to_prep,linelimit,
					linecount_sample,verbosity)
	return lines


if __name__ == "__main__":
	parser = parser_initialise()
	if parser is None:
		print(STATE_UNKNOWN_MESSAGE)
		exit(STATE_UNKNOWN)
	""" parse_args() default method exits status code 2
	but our local override to error() ensures argument
	problems get a STATE_UNKNOWN which is likely 3 (see constants) """
	args = parser.parse_args()

	""" Have reached this far so arguments expected by program look okay """
	if args.version:
		print('Plugin version is', VERSION)
		exit(STATE_OK)

	if args.timeout is not None:
		if args.timeout < 1:
			""" Zero or negative should be caught and when happens use default """
			args.timeout = TIMEOUT_DEFAULT

	program_binary = argv[0].strip()

	linkres = None
	#print(args.sartype)
	lines_n = []
	if args.sartype:

		#print(args.sartype)

		cconfig = yaml.safe_load(file(CHECK_CONFIG_FILE, 'rU'))
		cc_dict = cconfig['checks']
		#for check in cc_dict:
		#	print(check)

		sa = None
		lines_n = []
		if args.timeout == TIMEOUT_DEFAULT:
			# Likely no -t switch given so assume stdin can be read in
			with SysActivity(stdin.readlines()) as sa:
				if PYVERBOSITY is None or PYVERBOSITY > 1:
					sa.lines_print()
					lines_n = sa.lines_n_print(LINES_SAMPLE_SIZE)
				else:
					lines_n = sa.lines_n(LINES_SAMPLE_SIZE)
		else:
			# Use fileinput way of obtaining input lines
			flines = fileinput_lines(fileinput.input(files=args.files),
						args.maxlines,20,PYVERBOSITY)
			with SysActivity(flines) as sa:
				if PYVERBOSITY is None or PYVERBOSITY > 1:
					sa.lines_print()
					lines_n = sa.lines_n_print(LINES_SAMPLE_SIZE)
				else:
					lines_n = sa.lines_n(LINES_SAMPLE_SIZE)

		if sa is None:
			print(STATE_UNKNOWN_MESSAGE)
			exit(STATE_UNKNOWN)
		elif len(lines_n) < LINES_SAMPLE_SIZE:
			print("{0} fewer than {1} lines passed".format(NAGIOS_STEM_CRITICAL_SA,
									LINES_SAMPLE_SIZE))
			exit(STATE_CRITICAL)
		elif len(sa.line_n_labels) < 3:
			explanation_text = 'System Activity malformed or misunderstood.'
			print("CRITICAL: {0}".format(explanation_text))
			exit(STATE_CRITICAL)
		elif PYVERBOSITY is None or PYVERBOSITY > 1:
			print(sa.line_n_labels)
		else:
			pass

		fc4 = sa.field_checks(cc_dict)
		if 0 == sum(fc4[:2]):
			print("OK: {0}".format(SA_PASSED_ALL))
			exit(STATE_OK)
		elif fc4[1] > 0:
			print("CRITICAL: {0}".format(fc4[3]))
			exit(STATE_CRITICAL)
		else:
			print("WARNING: {0}".format(fc4[2]))
			exit(STATE_WARNING)

	else:
		print(STATE_UNKNOWN_MESSAGE)
		exit(STATE_UNKNOWN)

	if len(lines_n) < 1:
		exit(110)
	exit(0)
