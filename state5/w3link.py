#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2017, Gary Wright wrightsolutions.co.uk/contact
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the copyright holder nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

""" Nagios plugin to example a html response and
check it contains a link to a subdomain of w3.org """

from __future__ import print_function

STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3
STATE_DEPENDENT=4
STATE_UNKNOWN_MESSAGE='STATE UNKNOWN'
DESCRIPTION='Check HTTP page contains a link'

try:
    from argparse import ArgumentParser, RawTextHelpFormatter
    import subprocess
except Error,e:
    print(STATE_UNKNOWN_MESSAGE)
    exit(STATE_UNKNOWN)

VERSION='0.1'

class ArgParser(ArgumentParser):
    def error(self, message):
        #self.print_help()
        print(STATE_UNKNOWN_MESSAGE, ': ', message)
        exit(STATE_UNKNOWN)

def parser_initialise():
    par = None
    par = ArgParser(description=DESCRIPTION,formatter_class=RawTextHelpFormatter)
    par.add_argument('-V', '--version', help='Show plugin version',
                     action='store_true')
    par.add_argument('-H', '--hostname', help='Hostname',
                     action='store', dest='hostname', required=True)
    par.add_argument('-s', '--subdomain', help='Subdomain of w3',
                     action='store', dest='subdomain', default='validator')
    par.add_argument('-t', '--timeout', help='Plugin timeout',
                     action='store', dest='timeout', type=int, default=4)
    return par

def link_list_contains(response_raw,linkdom):
    linkres = False
    import re
    re_links = re.compile('<a href="?(.*?)"?>',re.IGNORECASE)
    link_list = re_links.findall(response_raw)
    link_list_stripped = [ lnk.strip('http://') for lnk in link_list ]
    for lnk in link_list_stripped:
        try:
            link = lnk.split('/')[0]
            if linkdom == link:
                linkres = True
        except:
            linkres = False
    return linkres

def check_for_link(hostname_bare,linkdom='validator.w3.org',
                   timeout=4,max_kb=None):
    """ Returns 0 if all okay (link was found)
    otherwise returns the non-zero value of max_kb
    Query max_kb on failure to see the upper limit on kilobytes used
    when the fetch request was issued.

    Returns False in exceptional situation where an error encountered
    that should probably trigger a STATE_CRITICAL.

    BeautifulSoup and lxml do a much better job of html parsing, but here
    the goal was to not be dependent on any extra Python modules
    being locally installed.

    max_kb is a future enhancement but is included in method signature.
    """
    if max_kb is None:
        max_kb = 500
    elif max_kb < 2:
        max_kb = 2
    elif max_kb > 1000:
        max_kb = 1000
    assert max_kb > 0
    linkres = max_kb
    from urllib2 import Request,HTTPError,URLError,urlopen
    from socket import timeout as tmout
    content = None
    hostname_with_protocol = "http://{0}".format(hostname_bare)
    response_content=None
    try:
        request = Request(hostname_with_protocol)
        response = urlopen(request, timeout=4)
        response_content = response.read()
    except HTTPError, e:
        linkres = max_kb
    except URLError, e:
        linkres = False
    except tmout, e:
        linkres = None
    else:
        linkres = 0
    if response_content is not None:
        if link_list_contains(response_content,linkdom):
            linkres = 0
        else:
            linkres = max_kb
    return linkres

if __name__ == "__main__":
    parser = parser_initialise()
    if parser is None:
        print(STATE_UNKNOWN_MESSAGE)
        exit(STATE_UNKNOWN)
    """ parse_args() default method exits status code 2
    but our local override to error() ensures argument
    problems get a STATE_UNKNOWN which is likely 3 (see constants) """
    args = parser.parse_args()

    """ Have reached this far so arguments expected by program look okay """
    if args.version:
        print('Plugin version is', VERSION)
        exit(STATE_OK)

    if args.timeout is not None:
        if args.timeout < 1:
            """ Zero or negative should be caught and when happens use default """
            args.timeout = 4

    linkres = None
    if args.hostname and args.subdomain and args.timeout:

        linkdomain="{0}.w3.org".format(args.subdomain)
        linkres = check_for_link(args.hostname,linkdomain,args.timeout)
        if linkres is None:
            print(STATE_UNKNOWN_MESSAGE)
            exit(STATE_UNKNOWN)
        elif linkres is False:
            explanation_text = 'URL malformed or dead'
            explanation_text += ' while attempting to search remote for'
            print("CRITICAL: w3link - {0} {1}".format(explanation_text,linkdomain))
            exit(STATE_CRITICAL)
        elif linkres == 0:
            print("OK: w3link Found ".format(linkdomain))
            exit(STATE_OK)
        else:
            print("CRITICAL: w3link did not find {0}".format(linkdomain))
            exit(STATE_CRITICAL)

    else:
        parser.error('Timeout arg error or other arg issue')


